export default {
  app: {
    HOST: process.env.APP_HOST || '0.0.0.0',
    PORT: process.env.APP_PORT || 3000,
  },
  // db: {
  //   HOST: process.env.DB_HOST || 'idb01lab1.ra.com',
  //   DATABASE: process.env.DB_NAME || 'phoenix',
  //   PORT: process.env.DB_PORT || 3306,
  //   USER: process.env.DB_USER || 'root',
  //   PASSWORD: process.env.DB_PASS || '',
  // },
};
