import { NextFunction, Request, Response } from 'express';
import { deleteCityValidator, getCityValidator, postCityValidator, putCityValidator } from './city.validators';
import { deleteCityService, getCityService, postCityService, putCityService } from './city.services';

const getCityController = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const validReq = getCityValidator(req.body);
    const result = await getCityService(validReq);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

const postCityController = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const validReq = postCityValidator(req.body);
    const result = await postCityService(validReq);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};
const putCityController = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const validReq = putCityValidator(req.body);
    const result = await putCityService(validReq);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};
const deleteCityController = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const validReq = deleteCityValidator(req.body);
    const result = await deleteCityService(validReq);
    res.status(200).json(result);
  } catch (error) {
    next(error);
  }
};

export { getCityController, putCityController, postCityController, deleteCityController };
