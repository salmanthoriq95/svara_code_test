export interface IGetCityPayload {
  roleId: number;
}

export interface IPostCityPayload {
  roleId: number;
  name: string;
}

export interface IPutCityPayload {
  roleId: number;
  id: string;
  name: string;
}

export interface IDeleteCityPayload {
  roleId: number;
  id: string;
}
