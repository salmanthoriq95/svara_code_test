const getAllCity = async () => {
  return [
    {
      id: '1asd123',
      name: 'city1',
    },
    {
      id: '1asd123',
      name: 'city2',
    },
  ];
};

const getSingleCity = async (id: string) => {
  return {
    id: '1asd123',
    name: 'city1',
  };
};

const addSingleCity = async (name: string) => {
  return [
    {
      id: '1asd123',
      name: 'city1',
    },
  ];
};

const editSingleCity = async (id: string, name: string) => {
  return [
    {
      id: '1asd123',
      name: 'city1',
    },
  ];
};

const softDelSingleCity = async (id: string) => {
  return [
    {
      id: '1asd123',
      name: 'city1',
    },
  ];
};

export { getAllCity, getSingleCity, addSingleCity, editSingleCity, softDelSingleCity };
