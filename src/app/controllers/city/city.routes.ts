import { Router } from 'express';
import { deleteCityController, getCityController, postCityController, putCityController } from './city.controllers';

const router = Router();

router.get('/', getCityController);
router.post('/', postCityController);
router.put('/', putCityController);
router.delete('/', deleteCityController);

export default router;
