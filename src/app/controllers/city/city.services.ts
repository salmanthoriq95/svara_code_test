import { IDeleteCityPayload, IGetCityPayload, IPostCityPayload, IPutCityPayload } from './city.interfaces';
import { addSingleCity, editSingleCity, getAllCity, softDelSingleCity } from './city.queries';

const getCityService = async (payload: IGetCityPayload) => {
  return await getAllCity();
};
const postCityService = async (payload: IPostCityPayload) => {
  return await addSingleCity(payload.name);
};
const putCityService = async (payload: IPutCityPayload) => {
  return await editSingleCity(payload.id, payload.name);
};
const deleteCityService = async (payload: IDeleteCityPayload) => {
  return await softDelSingleCity(payload.id);
};

export { getCityService, postCityService, putCityService, deleteCityService };
