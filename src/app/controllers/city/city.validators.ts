import Joi from 'joi';
import HttpException from '../../errors/HttpException';

const getCityValidator = (req: any) => {
  console.log(req);
  const schema = Joi.object({
    roleId: Joi.number().required(),
  });

  const result = schema.validate(req);

  if (result.error) {
    throw new HttpException(400, { success: false, message: 'Fail to validate!' });
  }

  return result.value;
};
const postCityValidator = (req: any) => {
  const schema = Joi.object({
    roleId: Joi.number().required(),
    name: Joi.string().required(),
  });

  const result = schema.validate(req);

  if (result.error) {
    throw new HttpException(400, { success: false, message: 'Fail to validate!' });
  }

  return result.value;
};
const putCityValidator = (req: any) => {
  const schema = Joi.object({
    roleId: Joi.number().required(),
    name: Joi.string().required(),
    id: Joi.string().required(),
  });

  const result = schema.validate(req);

  if (result.error) {
    throw new HttpException(400, { success: false, message: 'Fail to validate!' });
  }

  return result.value;
};

const deleteCityValidator = (req: any) => {
  const schema = Joi.object({
    roleId: Joi.number().required(),
    id: Joi.string().required(),
  });

  const result = schema.validate(req);

  if (result.error) {
    throw new HttpException(400, { success: false, message: 'Fail to validate!' });
  }

  return result.value;
};

export { getCityValidator, postCityValidator, putCityValidator, deleteCityValidator };
