export interface HttpData {
  message: string;
  success: boolean;
}
class HttpExpection extends Error {
  statusCode: number;
  success: boolean;
  constructor(statusCode: number, httpData: HttpData) {
    super(httpData.message);
    this.name = 'HttpExpection';
    this.statusCode = statusCode;
    this.success = httpData.success;
  }
}

export default HttpExpection;
