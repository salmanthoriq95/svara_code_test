import { Application } from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import errorHandler from './errors';
import routes from './routes';

export default (app: Application): void => {
  // Middleware
  app.use(helmet());
  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  // Api Handling
  Object.keys(routes).forEach((key) => {
    app.use('/', (routes as any)[key]);
  });

  // error handler
  app.use(errorHandler);

  // Unhandling Rejection Expection
  process.on('unhandledRejection', (reason: string, p: Promise<any>) => {
    console.log(p);
    throw reason;
  });

  process.on('uncaughtException', (error: Error) => {
    console.log(error);
    process.exit(1);
  });
};
