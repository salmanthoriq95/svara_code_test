import { Router } from 'express';
import cityRoutes from '../controllers/city/city.routes';

const router = Router();

export default {
  city: router.use('/city', cityRoutes),
  // auth: router.use('/auth', cityRoutes),
  // account: router.use('/account', cityRoutes),
};
