import express from 'express';
import appLoader from './app';
import config from './app/config';

const app = express();
appLoader(app);
app.listen(config.app.PORT, () => {
  console.log(`Listening on port ${config.app.PORT}`);
});
